# Solebull's PvP/Faction Minetest game

## En tant que joueur

Ce jeu minetest est basé sur
[Cobalt](https://forum.minetest.net/viewtopic.php?f=50&t=16034) 

1. Télecharger  [Minetest](https://www.minetest.net/downloads/).
   De préference, au minimum, la version 5.0.0.

2. Connectez vous au serveur nommé *Solebull*. Avant de vous connecter
   laissez nous un message contenant votre pseudo dans le chat : ce serveur
   utilise le mod *whitelist*.

## En tant que développeur

Si vous souhaitez contribuer au développement du jeu, il vous faut utiliser
un client `git` pour cloner le dépôt :

Allez dans le répertoire minetest/games/ puis :

	git clone https://gitlab.com/solebull/minetest-pvp.git

Et eventuellement, un mode d'edition `emacs` : `sudo apt install lua-mode`.

## Débogage

Pour cycler sur les différents écrans de déboguage, presser F5. L'affichage en
wireframe peut être très utile.

### Documentation

J'essaye de documenter tant que possible le code des mods. La documentation
est génerée par `doxygen`. Mais avant tout, vous devrez installer
`https://github.com/alecchen/doxygen-lua` pour permettre a doxygen de 
comprendre le code lua.

Son installation demande plusieurs modules perl :

	sudo apt install libmodule-install-perl

Suivant l'emplacement de `lua2dox`, vous devrez mettre a jour la variable
suivante de Doxyfile :

	FILTER_PATTERNS = *.lua=/path/to/your/lua2dox

puis

	doxygen
	
puis ouvrir `html/index.html`.

## En tant qu'administrateur

Après la géneration de la map, il faut y définir une *safe zone PVP* autout du
point de spawn. Le reste de la carte sera PVP par défaut.

Il faut définir le centre du spawn (pos1) et le point de la zone le plus éloigné
(pos2) :

	/pvp_areas pos1
	/pvp_areas pos2
	/pvp_areas set


## Installation sous Debian GNU/Linux Stretch

La version 0.4.16 n'est pas dans les dépôts Debian Stretch. Il faut
donc le compiler manuellement :

	sudo apt install build-essential libirrlicht-dev cmake libbz2-dev \
	  libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev libvorbis-dev     \
	  libluajit-5.1-dev
	git clone https://github.com/minetest/minetest.git
	cd minetest
	git checkout 0.4.16
	cd build
	cmake ..
	make
	sudo checkinstall --pkgname=minetest --fstrans=no --backup=no     \
	  --pkgversion="0.4.16" --deldoc=yes

A partir de Debian Buster, la version des dépôts officielle fonctionne
parfaitement.

Ensuite, cloner et instaler minetest-pvp dans le répertoire
*~/.minetest/games*. Ce repertoire peut ne pas exister si vous n'avez pas 
encore lancé *minetest*. Pour le créer lancez la comande suivante :

	mkdir -p ~/.minetest/games
	cd ~/.minetest/games
	git clone git@gitlab.com:solebull/minetest-pvp.git
	
	
## Pour installer le lien Mumble/Minetest

Le *lien Mumble/Minetest* permet de profiter d'une partie vocale 
spacialisée : si la personne vous parlant dans *mumble* est a droite, 
vous l'entendrez dans votre haut-parleur droit.

Pour l'installer, il vous faut d'abord vous diriger ici : 
https://github.com/chipgw/minetest-mumble-wrapper/releases

Ensuite, vérifier que votre Mumble est bien en marche et que votre dossier 
"minetest"(et non minetest-0.4.17.1 sinon cela ne marchera pas) se trouve dans 
votre dossier C:\Program Files .

Maintenant(dans le lien github), téléchargez le Source code selon votre version.
Décompressez le fichier, puis allez dans 
minetest-mumble-wrapper-0.1 -> minetestmod .

Prenez le fichier "mumble", puis glissez le dans le dossier "clientmods" de 
votre jeu Minetest. 

Ensuite, glissez dans ce même dossier ce fichier mods.conf : 
http://www.mediafire.com/file/ryzaicj0a4nr0d4/mods.conf/file

Démarrez maintenant Minetest, allez dans Réglages -> Réglages avancés puis
dans la barre de recherche, tapez "client" et assurez vous que
"Personnalisation client" est activé. Fermez Minetest.

Téléchargez maintenant le minetest-mumble-wrapper selon votre version(toujours
dans le lien github). Décompressez le fichier(glissez le de préférence dans le 
dossier bin du jeu), et exécutez le. 

Si votre Mumble est en marche et que votre Minetest se trouve ici :
C:\Program Files\minetest\bin\minetest.exe , tout devrait normalement marcher
lors de l'exécution du .exe.

Et maintenant, profitez du jeu avec une nouvelle dimension!

# Recettes
## Cisailles (Steel Shears)

Il s'agit des cisailles, utilisées pour récupérer la laine des moutons

- steel:none
- stick:steel

## Double coffre

Le deuxième coffre doit être placé sutr le côté du premier tout en appuyant
sur <kbd>Shift</kbd>.

## Envoyer vers l'inventaire/stacker

Lorsqu'on click sur <kbd>Shift</kbd> + <kbd>click</kbd>, on envoi rapidement
vers l'inventaire en stackant autant que possible.

## Sprint

Courez en appuyant sur <kbd>E</kbd> pour sprinter ou nager rapidement.

# Mapper

Le *mapper* utilisé côté serveur est 
[minetestmapper](https://github.com/minetest/minetestmapper).

## Compilation

	sudo apt install cmake libgd-dev checkinstall
	git clone https://github.com/minetest/minetestmapper.git
	cd minetestmapper/
	mkdir build
	cd build/
	cmake ..
	make
	sudo checkinstall --pkgname minetestmapper

## Utilisation

	minetestmapper -i ~/.minetest/worlds/MyWorld/ -o map.png

# Troubleshooting
## FPS très bas

Si les FPS rendent le jeu dificilement jouable (10-15 FPS) :

1. Aller dans *settings*;
2. Désactiver tous les *shaders*;
3. **Redémarrer le jeu**.

# Factions

La gestion des factions est assutrée par 
[factions](https://github.com/agrecascino/factions). 
Beaucoup de questions restent sans réponses concernant ce mod.

## Parcelles

La protection fonctionne par parcelles. Chaque faction peut revendiquer 
(*claim*) une parcelle suivant sa puissance.

## Faction chest

Les coffres de guild (*faction chest*) ne peuvent être ouvert si ils sont dans 
une parcelle appartenant a la guilde

## Minimap

Pour afficher ou changer le zoom de la minimap : F9.

## Commandes

Voici la liste des commandes implémentées. Pour y accéder taper 
`/factions <cmd>` :

    claim : Claim the plot of land you're on
    unclaim : Unclaim the plot of land you're on.
    list : List all registered factions
    version : Displays mod version.
    info : Shows a faction's description
    leave : Leave your faction
    kick: Kick a player from your faction
    create : Create a new faction
    join : Join a faction
    disband : Disband your faction
    close : Make your faction invite-only
    open : Allow any player to join your faction
    description : Set your faction's description
    invite : Invite a player to your faction
    uninvite : Revoke a player's invite
    delete : Delete a faction
    ranks : List ranks within your faction (eader, member, moderator)
    who : List players in your faction, and their ranks.
    newrank : Add a new rank
    delrank : Replace and delete a rank
    setspawn : Set the faction's spawn
    where : See whose parcel you stand on
    help : Shows help for commands
    spawn : Shows your faction's spawn
    promote : Promotes a player to a rank
    power : Display your faction's power
    setbanner : Sets the banner you're on as the faction's banner
    convert : Load factions in the old format
    free : Forcefully frees a parcel
    chat : Send a message to your faction's members
    forceupdate : Forces an update tick
    which : Gets a player's faction
    setleader : Set a player as a faction's leader
    setadmin : Make a faction an admin faction
    resetpower : Reset a faction's power
    obliterate : Remove all factions
    getspawn : Get a faction's spawn
    whoin : Get all members of a faction
    stats : Get stats of a faction
    seen : Check the last time a faction had a member logged in


## Server news

Le fichier affiché par le mod *server_news* est `mods/server_news/news.txt`.
