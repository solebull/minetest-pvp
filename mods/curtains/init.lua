--Black Curtain
minetest.register_node("curtains:black_closed", {
        description = "Black Curtain",
        tiles = {"wool_black.png"},
        inventory_image = "wool_black.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:black_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:black_open", param2 = 2})
      elseif node.name == "curtains:black_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:black_open", param2 = 3})
      elseif node.name == "curtains:black_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:black_open", param2 = 4})
      elseif node.name == "curtains:black_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:black_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:black_open", {
        description = "Black Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:black_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:black_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:black_closed", param2 = 2})
      elseif node.name == "curtains:black_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:black_closed", param2 = 3})
      elseif node.name == "curtains:black_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:black_closed", param2 = 4})
      elseif node.name == "curtains:black_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:black_closed", param2 = 5})
   end
end
})

--Blue Curtain
minetest.register_node("curtains:blue_closed", {
        description = "Blue Curtain",
        tiles = {"wool_blue.png"},
        inventory_image = "wool_blue.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:blue_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:blue_open", param2 = 2})
      elseif node.name == "curtains:blue_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:blue_open", param2 = 3})
      elseif node.name == "curtains:blue_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:blue_open", param2 = 4})
      elseif node.name == "curtains:blue_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:blue_open", param2 = 5})
        end
end
})

minetest.register_node("curtains:blue_open", {
        description = "Blue Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:blue_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:blue_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:blue_closed", param2 = 2})
      elseif node.name == "curtains:blue_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:blue_closed", param2 = 3})
      elseif node.name == "curtains:blue_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:blue_closed", param2 = 4})
      elseif node.name == "curtains:blue_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:blue_closed", param2 = 5})
   end
end
})

--Brown Curtain
minetest.register_node("curtains:brown_closed", {
        description = "Brown Curtain",
        inventory_image = "wool_brown.png",
        tiles = {"wool_brown.png"},
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:brown_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:brown_open", param2 = 2})
      elseif node.name == "curtains:brown_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:brown_open", param2 = 3})
      elseif node.name == "curtains:brown_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:brown_open", param2 = 4})
      elseif node.name == "curtains:brown_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:brown_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:brown_open", {
        description = "Brown Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:brown_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:brown_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:brown_closed", param2 = 2})
      elseif node.name == "curtains:brown_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:brown_closed", param2 = 3})
      elseif node.name == "curtains:brown_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:brown_closed", param2 = 4})
      elseif node.name == "curtains:brown_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:brown_closed", param2 = 5})
   end
end
})

--Cyan Curtain
minetest.register_node("curtains:cyan_closed", {
        description = "Cyan Curtain",
        tiles = {"wool_cyan.png"},
        inventory_image = "wool_cyan.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:cyan_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:cyan_open", param2 = 2})
      elseif node.name == "curtains:cyan_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:cyan_open", param2 = 3})
      elseif node.name == "curtains:cyan_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:cyan_open", param2 = 4})
      elseif node.name == "curtains:cyan_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:cyan_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:cyan_open", {
        description = "Cyan Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:cyan_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:cyan_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:cyan_closed", param2 = 2})
      elseif node.name == "curtains:cyan_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:cyan_closed", param2 = 3})
      elseif node.name == "curtains:cyan_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:cyan_closed", param2 = 4})
      elseif node.name == "curtains:cyan_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:cyan_closed", param2 = 5})
   end
end
})

--Dark Green Curtain
minetest.register_node("curtains:dark_green_closed", {
        description = "Dark Green Curtain",
        tiles = {"wool_dark_green.png"},
        inventory_image = "wool_dark_green.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:dark_green_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:dark_green_open", param2 = 2})
      elseif node.name == "curtains:dark_green_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:dark_green_open", param2 = 3})
      elseif node.name == "curtains:dark_green_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:dark_green_open", param2 = 4})
      elseif node.name == "curtains:dark_green_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:black_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:dark_green_open", {
        description = "Dark Green Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:dark_green_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:dark_green_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:dark_green_closed", param2 = 2})
      elseif node.name == "curtains:dark_green_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:dark_green_closed", param2 = 3})
      elseif node.name == "curtains:dark_green_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:dark_green_closed", param2 = 4})
      elseif node.name == "curtains:dark_green_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:dark_green_closed", param2 = 5})
   end
end
})

--Dark Grey Curtain
minetest.register_node("curtains:dark_grey_closed", {
        description = "Dark Grey Curtain",
        tiles = {"wool_dark_grey.png"},
        inventory_image = "wool_dark_grey.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:dark_grey_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:dark_grey_open", param2 = 2})
      elseif node.name == "curtains:dark_grey_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:dark_grey_open", param2 = 3})
      elseif node.name == "curtains:dark_grey_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:dark_grey_open", param2 = 4})
      elseif node.name == "curtains:dark_grey_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:dark_grey_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:dark_grey_open", {
        description = "Dark Grey Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:dark_grey_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:dark_grey_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:dark_grey_closed", param2 = 2})
      elseif node.name == "curtains:dark_grey_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:dark_grey_closed", param2 = 3})
      elseif node.name == "curtains:dark_grey_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:dark_grey_closed", param2 = 4})
      elseif node.name == "curtains:dark_grey_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:dark_grey_closed", param2 = 5})
   end
end
})

--Green Curtain
minetest.register_node("curtains:green_closed", {
        description = "Green Curtain",
        tiles = {"wool_green.png"},
        inventory_image = "wool_green.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:green_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:green_open", param2 = 2})
      elseif node.name == "curtains:green_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:green_open", param2 = 3})
      elseif node.name == "curtains:green_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:green_open", param2 = 4})
      elseif node.name == "curtains:green_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:green_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:green_open", {
        description = "Green Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:green_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:green_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:green_closed", param2 = 2})
      elseif node.name == "curtains:green_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:green_closed", param2 = 3})
      elseif node.name == "curtains:green_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:green_closed", param2 = 4})
      elseif node.name == "curtains:green_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:green_closed", param2 = 5})
   end
end
})

--Grey Curtain
minetest.register_node("curtains:grey_closed", {
        description = "Grey Curtain",
        tiles = {"wool_grey.png"},
        inventory_image = "wool_grey.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:grey_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:grey_open", param2 = 2})
      elseif node.name == "curtains:grey_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:grey_open", param2 = 3})
      elseif node.name == "curtains:grey_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:grey_open", param2 = 4})
      elseif node.name == "curtains:grey_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:grey_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:grey_open", {
        description = "Grey Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:grey_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:grey_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:grey_closed", param2 = 2})
      elseif node.name == "curtains:grey_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:grey_closed", param2 = 3})
      elseif node.name == "curtains:grey_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:grey_closed", param2 = 4})
      elseif node.name == "curtains:grey_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:grey_closed", param2 = 5})
   end
end
})

--Magenta Curtain
minetest.register_node("curtains:magenta_closed", {
        description = "Magenta Curtain",
        tiles = {"wool_magenta.png"},
        inventory_image = "wool_magenta.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:magenta_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:magenta_open", param2 = 2})
      elseif node.name == "curtains:magenta_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:magenta_open", param2 = 3})
      elseif node.name == "curtains:magenta_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:magenta_open", param2 = 4})
      elseif node.name == "curtains:magenta_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:magenta_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:magenta_open", {
        description = "Magenta Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:magenta_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:magenta_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:magenta_closed", param2 = 2})
      elseif node.name == "curtains:magenta_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:magenta_closed", param2 = 3})
      elseif node.name == "curtains:magenta_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:magenta_closed", param2 = 4})
      elseif node.name == "curtains:magenta_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:magenta_closed", param2 = 5})
   end
end
})

--Orange Curtain
minetest.register_node("curtains:orange_closed", {
        description = "Orange Curtain",
        tiles = {"wool_orange.png"},
        inventory_image = "wool_orange.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:orange_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:orange_open", param2 = 2})
      elseif node.name == "curtains:orange_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:orange_open", param2 = 3})
      elseif node.name == "curtains:orange_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:orange_open", param2 = 4})
      elseif node.name == "curtains:orange_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:orange_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:orange_open", {
        description = "Orange Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:orange_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:orange_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:orange_closed", param2 = 2})
      elseif node.name == "curtains:orange_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:orange_closed", param2 = 3})
      elseif node.name == "curtains:orange_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:orange_closed", param2 = 4})
      elseif node.name == "curtains:orange_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:orange_closed", param2 = 5})
   end
end
})

--Pink Curtain
minetest.register_node("curtains:pink_closed", {
        description = "Pink Curtain",
        tiles = {"wool_pink.png"},
        inventory_image = "wool_pink.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:pink_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:pink_open", param2 = 2})
      elseif node.name == "curtains:pink_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:pink_open", param2 = 3})
      elseif node.name == "curtains:pink_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:pink_open", param2 = 4})
      elseif node.name == "curtains:pink_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:pink_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:pink_open", {
        description = "Pink Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:pink_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:pink_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:pink_closed", param2 = 2})
      elseif node.name == "curtains:pink_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:pink_closed", param2 = 3})
      elseif node.name == "curtains:pink_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:pink_closed", param2 = 4})
      elseif node.name == "curtains:pink_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:pink_closed", param2 = 5})
   end
end
})

--Red Curtain
minetest.register_node("curtains:red_closed", {
        description = "Red Curtain",
        tiles = {"wool_red.png"},
        inventory_image = "wool_red.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:red_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:red_open", param2 = 2})
      elseif node.name == "curtains:red_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:red_open", param2 = 3})
      elseif node.name == "curtains:red_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:red_open", param2 = 4})
      elseif node.name == "curtains:red_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:red_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:red_open", {
        description = "Red Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:red_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:red_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:red_closed", param2 = 2})
      elseif node.name == "curtains:red_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:red_closed", param2 = 3})
      elseif node.name == "curtains:red_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:red_closed", param2 = 4})
      elseif node.name == "curtains:red_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:red_closed", param2 = 5})
   end
end
})

--Violet Curtain
minetest.register_node("curtains:violet_closed", {
        description = "Violet Curtain",
        tiles = {"wool_violet.png"},
        inventory_image = "wool_violet.png", 
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:violet_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:violet_open", param2 = 2})
      elseif node.name == "curtains:violet_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:violet_open", param2 = 3})
      elseif node.name == "curtains:violet_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:violet_open", param2 = 4})
      elseif node.name == "curtains:violet_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:violet_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:violet_open", {
        description = "Violet Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:violet_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:violet_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:violet_closed", param2 = 2})
      elseif node.name == "curtains:violet_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:violet_closed", param2 = 3})
      elseif node.name == "curtains:violet_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:violet_closed", param2 = 4})
      elseif node.name == "curtains:violet_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:violet_closed", param2 = 5})
   end
end
})

--White Curtain
minetest.register_node("curtains:white_closed", {
        description = "White Curtain",
        tiles = {"wool_white.png"},
        inventory_image = "wool_white.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:white_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:white_open", param2 = 2})
      elseif node.name == "curtains:white_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:white_open", param2 = 3})
      elseif node.name == "curtains:white_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:white_open", param2 = 4})
      elseif node.name == "curtains:white_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:white_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:white_open", {
        description = "White Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:white_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:white_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:white_closed", param2 = 2})
      elseif node.name == "curtains:white_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:white_closed", param2 = 3})
      elseif node.name == "curtains:white_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:white_closed", param2 = 4})
      elseif node.name == "curtains:white_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:white_closed", param2 = 5})
   end
end
})

--Yellow Curtain
minetest.register_node("curtains:yellow_closed", {
        description = "Yellow Curtain",
        tiles = {"wool_yellow.png"},
        inventory_image = "wool_yellow.png",
        paramtype = "light",
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
       if node.name == "curtains:yellow_closed" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:yellow_open", param2 = 2})
      elseif node.name == "curtains:yellow_closed" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:yellow_open", param2 = 3})
      elseif node.name == "curtains:yellow_closed" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:yellow_open", param2 = 4})
      elseif node.name == "curtains:yellow_closed" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:yellow_open", param2 = 5})
        end
end
})
 
minetest.register_node("curtains:yellow_open", {
        description = "Yellow Curtain",
        tiles = {"invisible.png"},
        paramtype = "light",    
        paramtype2 = "wallmounted",
        selection_box = {
                type = "wallmounted",
                --wall_top = = <default>
                --wall_bottom = = <default>
                --wall_side = = <default>
        },
        drawtype = "signlike",
        drop = "curtains:yellow_closed",
        walkable = false,
        groups = {oddly_breakable_by_hand=3,flammable=2,not_in_creative_inventory=1},
        sounds = default.node_sound_leaves_defaults(),
        on_rightclick = function(pos, node, clicker)
                playername = clicker:get_player_name()
                if minetest.is_protected(pos, playername) then
                        minetest.record_protection_violation(pos, playername)
                        return
                end
      if node.name == "curtains:yellow_open" and node.param2 == 2 then
      minetest.add_node(pos, {name="curtains:yellow_closed", param2 = 2})
      elseif node.name == "curtains:yellow_open" and node.param2 == 3 then
      minetest.add_node(pos, {name="curtains:yellow_closed", param2 = 3})
      elseif node.name == "curtains:yellow_open" and node.param2 == 4 then
      minetest.add_node(pos, {name="curtains:yellow_closed", param2 = 4})
      elseif node.name == "curtains:yellow_open" and node.param2 == 5 then
      minetest.add_node(pos, {name="curtains:yellow_closed", param2 = 5})
   end
end
})

--Crafting w/ wool

minetest.register_craft({
	output = 'curtains:black_closed 4',
	recipe = {
		{'wool:black', 'wool:black'},
		{'wool:black', 'wool:black'},
	}
})

minetest.register_craft({
	output = 'curtains:blue_closed 4',
	recipe = {
		{'wool:blue', 'wool:blue'},
		{'wool:blue', 'wool:blue'},
	}
})

minetest.register_craft({
	output = 'curtains:brown_closed 4',
	recipe = {
		{'wool:brown', 'wool:brown'},
		{'wool:brown', 'wool:brown'},
	}
})

minetest.register_craft({
	output = 'curtains:cyan_closed 4',
	recipe = {
		{'wool:cyan', 'wool:cyan'},
		{'wool:cyan', 'wool:cyan'},
	}
})

minetest.register_craft({
	output = 'curtains:dark_green_closed 4',
	recipe = {
		{'wool:dark_green', 'wool:dark_green'},
		{'wool:dark_green', 'wool:dark_green'},
	}
})

minetest.register_craft({
	output = 'curtains:dark_grey_closed 4',
	recipe = {
		{'wool:dark_grey', 'wool:dark_grey'},
		{'wool:dark_grey', 'wool:dark_grey'},
	}
})

minetest.register_craft({
	output = 'curtains:green_closed 4',
	recipe = {
		{'wool:green', 'wool:green'},
		{'wool:green', 'wool:green'},
	}
})

minetest.register_craft({
	output = 'curtains:grey_closed 4',
	recipe = {
		{'wool:grey', 'wool:grey'},
		{'wool:grey', 'wool:grey'},
	}
})


minetest.register_craft({
	output = 'curtains:magenta_closed 4',
	recipe = {
		{'wool:magenta', 'wool:magenta'},
		{'wool:magenta', 'wool:magenta'},
	}
})

minetest.register_craft({
	output = 'curtains:orange_closed 4',
	recipe = {
		{'wool:orange', 'wool:orange'},
		{'wool:orange', 'wool:orange'},
	}
})

minetest.register_craft({
	output = 'curtains:pink_closed 4',
	recipe = {
		{'wool:pink', 'wool:pink'},
		{'wool:pink', 'wool:pink'},
	}
})

minetest.register_craft({
	output = 'curtains:red_closed 4',
	recipe = {
		{'wool:red', 'wool:red'},
		{'wool:red', 'wool:red'},
	}
})

minetest.register_craft({
	output = 'curtains:violet_closed 4',
	recipe = {
		{'wool:violet', 'wool:violet'},
		{'wool:violet', 'wool:violet'},
	}
})

minetest.register_craft({
	output = 'curtains:white_closed 4',
	recipe = {
		{'wool:white', 'wool:white'},
		{'wool:white', 'wool:white'},
	}
})

minetest.register_craft({
	output = 'curtains:yellow_closed 4',
	recipe = {
		{'wool:yellow', 'wool:yellow'},
		{'wool:yellow', 'wool:yellow'},
	}
})

--Crafting w/ white curtains and dyes

minetest.register_craft({
        output = 'curtains:black_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:black'},
        }
})

minetest.register_craft({
        output = 'curtains:blue_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:blue'},
        }
})

minetest.register_craft({
        output = 'curtains:brown_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:brown'},
        }
})

minetest.register_craft({
        output = 'curtains:cyan_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:cyan'},
        }
})

minetest.register_craft({
        output = 'curtains:dark_green_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:dark_green'},
        }
})

minetest.register_craft({
        output = 'curtains:dark_grey_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:dark_grey'},
        }
})

minetest.register_craft({
        output = 'curtains:green_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:green'},
        }
})

minetest.register_craft({
        output = 'curtains:grey_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:grey'},
        }
})

minetest.register_craft({
        output = 'curtains:magenta_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:magenta'},
        }
})

minetest.register_craft({
        output = 'curtains:orange_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:orange'},
        }
})

minetest.register_craft({
        output = 'curtains:pink_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:pink'},
        }
})

minetest.register_craft({
        output = 'curtains:red_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:red'},
        }
})

minetest.register_craft({
        output = 'curtains:violet_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:violet'},
        }
})

minetest.register_craft({
        output = 'curtains:yellow_closed',
        recipe = {
                {'curtains:white_closed'},
                {'dye:yellow'},
        }
})