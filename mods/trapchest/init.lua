--TNT Chest
--When someone try to open, it will change to <BURNING> TNT!
minetest.register_node("trapchest:tntchest", {
	description = "TNT Chest",
	tiles = {"default_chest_top.png", "default_chest_top.png", "default_chest_side.png",
		"default_chest_side.png", "default_chest_side.png", "default_chest_front.png"},
	paramtype2 = "facedir",
	groups = {choppy=2,oddly_breakable_by_hand=2},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_wood_defaults(),

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext", "Chest")
	end,
	on_rightclick = function(pos, node, clicker)
		minetest.chat_send_player(clicker:get_player_name(), " * Oops! TNT Trap! *")
		minetest.sound_play("tnt_ignite", {pos=pos})
		minetest.set_node(pos, {name="tnt:tnt_burning"})
		minetest.get_node_timer(pos):start(4)
	end,
})

--Fire Chest
--When someone try to open, it will change to 'Fire'.
minetest.register_node("trapchest:firechest", {
	description = "FireTrap Chest",
	tiles = {"default_chest_top.png", "default_chest_top.png", "default_chest_side.png",
		"default_chest_side.png", "default_chest_side.png", "default_chest_front.png"},
	paramtype2 = "facedir",
	groups = {choppy=2,oddly_breakable_by_hand=2},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_wood_defaults(),

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext", "Chest")
	end,
	on_rightclick = function(pos, node, clicker)
		minetest.chat_send_player(clicker:get_player_name(), " * Oops! Fire Trap! *")
		minetest.set_node(pos, {name="fire:basic_flame"})
	end,
})

--Water Chest
--When someone try to open, it will change to water source.
minetest.register_node("trapchest:waterchest", {
	description = "WaterTrap Chest",
	tiles = {"default_chest_top.png", "default_chest_top.png", "default_chest_side.png",
		"default_chest_side.png", "default_chest_side.png", "default_chest_front.png"},
	paramtype2 = "facedir",
	groups = {choppy=2,oddly_breakable_by_hand=2},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_wood_defaults(),

	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext", "Chest")
	end,
	on_rightclick = function(pos, node, clicker)
		minetest.chat_send_player(clicker:get_player_name(), " * Oops! Water Trap! *")
		minetest.set_node(pos, {name="default:water_source"})
	end,
})

--Recipe. [ Chest + "something" ]
--Recraft is possible. (*If it do NOT Trigger.)
minetest.register_craft({
	output = "trapchest:tntchest",
	type = "shapeless",
	recipe = {"default:chest", "tnt:tnt"},
})

minetest.register_craft({
	output = "tnt:tnt",
	type = "shapeless",
	recipe = {"trapchest:tntchest"},
	replacements = {
		{"trapchest:tntchest","default:chest"},
	}
})

minetest.register_craft({
	output = "trapchest:firechest",
	type = "shapeless",
	recipe = {"default:chest", "tnt:gunpowder"},
})

minetest.register_craft({
	output = "tnt:gunpowder",
	type = "shapeless",
	recipe = {"trapchest:firechest"},
	replacements = {
		{"trapchest:firechest","default:chest"},
	}
})

minetest.register_craft({
	output = "trapchest:waterchest",
	type = "shapeless",
	recipe = {"default:chest", "bucket:bucket_water"},
	-- Consume Bucket. NOT return empty Bucket.
})

minetest.register_craft({
	output = "bucket:bucket_water",
	type = "shapeless",
	recipe = {"trapchest:waterchest"},
	replacements = {
		{"trapchest:waterchest","default:chest"},
	}
})

