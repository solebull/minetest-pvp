This is a continuation of my (Sokomines) fork of Nores mg mapgen.
The fork can be found under https://github.com/Sokomine/mg

# Local modifications

This mod is usable in PVP/faction because we're completely removing chests and
their loot in NPC villages. I fact, we're remplacing them with bookshelf
in `replacements.lua` around L360 :

	table.insert( replacements, {'default:chest', 'default:bookshelf' });

	
