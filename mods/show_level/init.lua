-- Permanently shows the player's level

local elemid = nil -- Needed to update existing HUD element

local update_level = function(playername)
   local level = ""..xpro.get_player_lvl(playername)
   local player = minetest.get_player_by_name(playername)
   
   if elemid == nil then
      -- Create HUD element
      elemid = player:hud_add({
	    hud_elem_type = "text",
	    position      = {x = 0.5, y = 1}, -- from the screen bottom 
	    offset        = {x = 0,   y = -160},
	    text          = level,
	    alignment     = {x = 0, y = 0},    -- center aligned
	    scale         = {x = 100, y = 30}, -- one line of text
	    number        = 0x00FF00,
      })
   else
      -- Update
      player:hud_change(elemid, "text", level)
   end
end

-- Showing level when a player join
minetest.register_on_joinplayer(function(player)
      local name = player:get_player_name()
      update_level(name)
end)

-- Update when adding XPs
xpro.register_on_add_xp(function(name, xp_added, lvl_changed)
      update_level(name)
end)

-- Update when removing XPs
xpro.register_on_rem_xp(function(name, xp_removed, lvl_changed)
      update_level(name)
end)
