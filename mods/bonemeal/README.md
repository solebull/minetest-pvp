# Bonemeal

*Bonemeal* is crushed from bones found in dirt or by using player bones, 
Mulch is made from a tree trunk surrounded by leaves and Fertiliser is a 
mix of both, each of which can be used to quickly grow saplings, crops 
and grass/decoration, papyrus on top of dirt and cactus on sand. Support 
for ethereal saplings/crops, farming redo crops and moretrees saplings 
are included.

## Licence

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE v2. see *Licence.txt*.
