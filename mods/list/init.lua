-- Register the /list command used to print player list

listcmd = function (playername,parameter)

   local lst = "Joueurs connectés : "
   for _, player in pairs(minetest.get_connected_players()) do
      lst = lst..player:get_player_name().." "
   end
   minetest.chat_send_player(playername,lst)
end   

minetest.register_chatcommand("list", {
      params = "<cmd>",
      description = "list players",
      privs = { interact=true },
      func = listcmd,
    }
)
