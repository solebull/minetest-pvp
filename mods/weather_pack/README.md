weather-pack
=======================
Weather mod for Minetest (http://minetest.net/)

Weathers included
-----------------------
* light_rain, rain, heavy_rain
* snow
* thunder (works together with heavy_rain)

Commands
-----------------------
requires `weather_manager` privilege.

  * `start_weather <weather_code>` 
  * `stop_weather <weather_code>` 

Be aware that weather may not be visible for player until player is in right biome.

Dependencies
-----------------------
Thunder weather requres [lightning](https://github.com/minetest-mods/lightning) mod.

License of source code:
-----------------------
MIT

Authors of media files:
-----------------------

xeranas:

  * `happy_weather_heavy_rain_drops.png` - CC-0
  * `happy_weather_light_rain_raindrop_1.png` - CC-0
  * `happy_weather_light_rain_raindrop_2.png` - CC-0
  * `happy_weather_light_rain_raindrop_3.png` - CC-0
  * `happy_weather_light_snow_snowflake_1.png` - CC-0
  * `happy_weather_light_snow_snowflake_2.png` - CC-0
  * `happy_weather_light_snow_snowflake_3.png` - CC-0

inchadney (http://freesound.org/people/inchadney/):

  * `rain_drop.ogg` - CC-BY-SA 3.0 (cut from http://freesound.org/people/inchadney/sounds/58835/)

rcproductions54 (http://freesound.org/people/rcproductions54/):

  * `light_rain_drop.ogg` - CC-0 (http://freesound.org/people/rcproductions54/sounds/265045/)

uberhuberman

  * `heavy_rain_drop.ogg` - CC BY 3.0 (https://www.freesound.org/people/uberhuberman/sounds/21189/)
