--[[ \file factions_ui/init.lua 

A main and unique file of factions_ui mod.

--]]

--- Keep the module path
local modpath = minetest.get_modpath("factions_ui")

--- Send the given message to the given player
-- @param player  The player to send the message to
-- @param message The message to be sent
local send_error = function(player, message)
    minetest.chat_send_player(player, message)
end

--- Count and return the number of players in the given faction
--
-- @param faction A faction object
function fac_player_count(faction)
    local count = 0.
    for k, v in pairs(faction.players) do
        count = count + 1
    end
    return count
end


--- Return the formspec for the given faction index (or 0 if none)
--
-- @param faction_id The faction identifier
--
-- @return A formspec
get_factions_ui = function(faction_id)
   local list = factions.get_faction_list()
   local t = { }
   for k,f in pairs(list) do
      t[#t+1] = f
   end
   local facnames = table.concat(t, ",")

   local fname = ""
   local power = ""
   local members = ""
   local leader = ""
   local iotext = ""
   
   if faction_id > 0 then
      local faction = list[0]
      fname = list[faction_id];
      local faction = factions.get_faction(fname)
      power = faction.power..'/'..faction.maxpower
      members = fac_player_count(faction)
      leader = faction.leader or faction.players[0] or "---"
      if faction.join_free then
	 iotext = "false"
      else
	 iotext = "true"
      end

   end
   
   local formspec = [[
    label[0,0;Nom :]         label[2,0;]]..fname..[[]
    label[0,0.25;Leader :]     label[2,0.25;]]..leader..[[]
    label[0,0.5;Puissance :]   label[2,0.5;]]..power..[[]
    label[0,0.75;Membres :]     label[2,0.75;]]..members..[[]
    label[0,1;Invite-only :]     label[2,1;]]..iotext..[[]
    textlist[0,2;7,6;faclist;]]..facnames..[[]
    field[5,1;2,1;name;Nouvelle faction :;newFaction]
    button[5,8;2,1;create;Créer]
  ]]
   return formspec
end

--- Creates the new faction from the factions_UI formspec
--
-- @param name   The faction name.
-- @param player The new faction's leader.
--
-- @return true if the faction is created, false if an error occured.
create_faction_from_UI = function(name, player)
   if faction then
      send_error(player, "You are already in a faction.")
      return false
   end
   if factions.can_create_faction(name) then
      new_faction = factions.new_faction(name, nil)
      new_faction:add_player(player, new_faction.default_leader_rank)
      
      -- Manual fix ?
      new_faction.leader = player
      new_faction.players[player] = player
      factions.save()
      
      return true
   else
      send_error(player, "Faction cannot be created.")
      return false
   end
end
   
sfinv.register_page("sfinv:factions_ui", {
        title = "Factions",
	on_enter = function(self, player, context)
	   context.faction_idx = 0
	end,
        get = function(self, player, context)
	   return sfinv.make_formspec(player, context,
				      get_factions_ui(context.faction_idx),
				      false)
	   
        end,

	on_player_receive_fields = function(self, player, context, fields)
	   local event = minetest.explode_textlist_event(fields.faclist)
	   if event.type == "CHG" then
	      -- We're selecting a new faction name
	      context.faction_idx= event.index
	      sfinv.set_player_inventory_formspec(player, context)
	   elseif event.type == "INV" then
	      -- We clicked the Create button
	      local factionName = fields.name
	      local pn = player:get_player_name()
	      local leader = player
	      create_faction_from_UI(factionName, pn)
	      minetest.show_formspec(pn, "", "")  -- close the formspec
	   end
	end
})

-- Used to dump a lua table
function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
