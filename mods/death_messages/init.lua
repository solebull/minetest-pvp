--[[
death_messages - A Minetest mod which sends a chat message when a player dies.
Copyright (C) 2016  EvergreenTree

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-----------------------------------------------------------------------------------------------
local title = "Death Messages"
local version = "0.1.2"
local mname = "death_messages"
-----------------------------------------------------------------------------------------------
dofile(minetest.get_modpath("death_messages").."/settings.txt")
-----------------------------------------------------------------------------------------------

-- A table of quips for death messages.  The first item in each sub table is the
-- default message used when RANDOM_MESSAGES is disabled.

-- Each group of message if for multiplayer when y* message are for singleplayer
-- it is a "Vous "... version of the message.

local messages = {}

-- Lava death messages
messages.lava = {
	" a rencontré une boule de feu.",
	" a pensé que la lave est cool.",
	" ne pouvait pas résister à la lave.",
	" a creusé tout droit.",
	" ne savait pas que la lave est chaude."
}
messages.ylava = {
	" avez rencontré une boule de feu.",
	" pensez que la lave est cool ?",
	" ne pouviez pas résister à la lave.",
	" avez creusé tout droit.",
	" ne saviez pas que la lave est chaude."
}

-- Drowning death messages
messages.water = {
	" s'est noyé.",
	" a manqué d'air.",
	" a échoué aux cours de natation.",
	" a essayé de se faire passer pour une ancre.",
	" a oublié qu'il n'était pas un poisson.",
	" a fait trop de bulles.",
	" a passé trop de temps dans un lavabo."
}

messages.ywater = {
	" vous êtes noyé.",
	" manquez d'air.",
	" avez échoué aux cours de natation.",
	" avez essayé de vous faire passer pour une ancre.",
	" avez oublié qu'il vous n'étiez pas un poisson.",
	" avez fait trop de bulles.",
	" avez passé trop de temps dans un lavabo."
}

-- Burning death messages
messages.fire = {
	" a brûlé à souhait.",
	" a eu un peu trop chaud.",
	" s'est approché trop près du feu de camp.",
	" vient d'être rôti, style hot-dog.",
	" s'est brûlé."
}
messages.yfire = {
	" avez brûlé à souhait.",
	" avez eu un peu trop chaud.",
	" vous êtes approché trop près du feu de camp.",
	" venez d'être rôti, style hot-dog.",
	" vous êtes brûlé."
}

-- Other death messages
messages.other = {
	" est mort.",
	" a fait quelque chose de fatal.",
	" a abandonné la vie.",
	" est un peu mort maintenant.",
	" s'est évanoui de façon permanente.",
	" est mort d'impatience.",
	" a peut-être rencontré Séréna."
}
messages.yother = {
	" êtes mort.",
	" avez fait quelque chose de fatal.",
	" avez abandonné la vie.",
	" êtes un peu mort maintenant.",
	" vous êtes évanoui de façon permanente.",
	" êtes mort d'impatience.",
	" avez peut-être rencontré Séréna."
}

function get_message(mtype)
	if RANDOM_MESSAGES then
		return messages[mtype][math.random(1, #messages[mtype])]
	else
		return messages[1] -- 1 is the index for the non-random message
	end
end

minetest.register_on_dieplayer(function(player)
      local player_name = player:get_player_name()
      local node = minetest.registered_nodes[minetest.get_node(player:getpos()).name]
      local msg;
      
      if minetest.is_singleplayer() then
	 player_name = "Vous"
	 -- Death by lava
	 if node.groups.lava ~= nil then
	    msg = get_message("ylava")
	    -- Death by drowning
	 elseif player:get_breath() == 0 then
	    msg = get_message("ywater")
	    -- Death by fire
	 elseif node.name == "fire:basic_flame" then
	    msg = get_message("yfire")
	    -- Death by something else
	 else
	    msg = get_message("yother")
	 end
      else
	 -- Death by lava
	 if node.groups.lava ~= nil then
	    msg = get_message("lava")
	    -- Death by drowning
	 elseif player:get_breath() == 0 then
	    msg = get_message("water")
	    -- Death by fire
	 elseif node.name == "fire:basic_flame" then
	    msg = get_message("fire")
	    -- Death by something else
	 else
	    msg = get_message("other")
	 end
      end
      
      minetest.chat_send_all(player_name .. msg)

end)

-----------------------------------------------------------------------------------------------
print("[Mod] "..title.." ["..version.."] ["..mname.."] Loaded...")
-----------------------------------------------------------------------------------------------
