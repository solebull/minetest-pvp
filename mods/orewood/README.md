# OreWood

https://forum.minetest.net/viewtopic.php?f=9&t=12093

This mod adds ore generation to tree trunks, and also allows you to grow the logs into farmable plants. It is intended to ease progression in the game, if you enjoy difficulty then this mod is probably not for you. If you're sick of digging like I am, though...


Ore is generated in both regular and jungle trees. Junglewood has a higher chance of spawning ore. All logs except coalwood logs need to be smelted to get their resources, coalwood just needs to be crafted. Place any orewood log (doesn't matter if it is facing up or sideways) next to water and it will sprout papyrus-like plants that drop their respective resources when they are harvested. They grow at about the same rate as papyrus, and have a max grow height just like it as well, the code is pretty much the same. Both junglewood and oak wood grow plants at the same rate.

A huge thank you to /u/Thermal--Shock for suggesting harvestable plants.

Todo: support for other mods.

Changelog:

1.1.1 to 1.1.5: Made it to where log textures are loaded from the current texture pack, to imporve support for 3rd party packs and games like carbone. I also had the odd urge to make ores slightly more commonm again.

1.1 to 1.1.1: Desaturated diamond and gold textures so they don't look like neon paint on the trees. Slightly reduced spawn rate of diamond, gold and mese.

1.0 to 1.1: Orewood can now be placed near water to grow plants. Coalwood no longer just drops a coal lump, it must be crafted. Also fixed a typo in the names prior to the 1.1 release.

![alt tag](http://i.imgur.com/8AO956N.png)


![alt tag](http://i.imgur.com/j3DTlOr.png)


![alt tag](http://i.imgur.com/uPQnoxS.png)

