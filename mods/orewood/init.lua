-- Orewood Node Registry

minetest.register_node("orewood:ironwood", {
	description = "Ironwood",
	paramtype2 = "facedir",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^ironwood.png"},
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:copperwood", {
	description = "Copperwood",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^copperwood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:goldwood", {
	description = "Goldwood",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^goldwood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:coalwood", {
	description = "Coalwood",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^coalwood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:mesewood", {
	description = "Mesewood",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^mesewood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:diamondwood", {
	description = "Diamondwood",
	tiles = {"default_tree_top.png", "default_tree_top.png", "default_tree.png^diamondwood.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:ironwood_jungle", {
	description = "Jungle Ironwood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^ironwood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:copperwood_jungle", {
	description = "Jungle Copperwood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^copperwood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:mesewood_jungle", {
	description = "Jungle Mesewood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^mesewood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:diamondwood_jungle", {
	description = "Jungle Diamondwood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^diamondwood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:coalwood_jungle", {
	description = "Jungle Coalwood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^coalwood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node("orewood:goldwood_jungle", {
	description = "Jungle Goldwood",
	tiles = {"default_jungletree_top.png", "default_jungletree_top.png", "default_jungletree.png^goldwood_jungle.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree=1,choppy=2,oddly_breakable_by_hand=1,flammable=2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})



-- Crafts

minetest.register_craft({
	output = 'default:coal_lump',
	recipe = {
		{'orewood:coalwood'},
	}
})

minetest.register_craft({
	output = 'default:coal_lump',
	recipe = {
		{'orewood:coalwood_jungle'},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "default:steel_ingot",
	recipe = "orewood:ironwood",
})

minetest.register_craft({
	type = "cooking",
	output = "default:copper_ingot",
	recipe = "orewood:copperwood",
})

minetest.register_craft({
	type = "cooking",
	output = "default:gold_ingot",
	recipe = "orewood:goldwood",
})

minetest.register_craft({
	type = "cooking",
	output = "default:diamond",
	recipe = "orewood:diamondwood",
})

minetest.register_craft({
	type = "cooking",
	output = "default:mese_crystal",
	recipe = "orewood:mesewood",
})

minetest.register_craft({
	type = "cooking",
	output = "default:steel_ingot",
	recipe = "orewood:ironwood_jungle",
})

minetest.register_craft({
	type = "cooking",
	output = "default:copper_ingot",
	recipe = "orewood:copperwood_jungle",
})

minetest.register_craft({
	type = "cooking",
	output = "default:gold_ingot",
	recipe = "orewood:goldwood_jungle",
})

minetest.register_craft({
	type = "cooking",
	output = "default:diamond",
	recipe = "orewood:diamondwood_jungle",
})

minetest.register_craft({
	type = "cooking",
	output = "default:mese_crystal",
	recipe = "orewood:mesewood_jungle",
})

-- Gen

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:ironwood",
		wherein        = "default:tree",
		clust_scarcity = 15*15*15,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:copperwood",
		wherein        = "default:tree",
		clust_scarcity = 14*14*14,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:goldwood",
		wherein        = "default:tree",
		clust_scarcity = 17*17*17,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:diamondwood",
		wherein        = "default:tree",
		clust_scarcity = 19*19*19,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})


minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:coalwood",
		wherein        = "default:tree",
		clust_scarcity = 13*13*13,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:mesewood",
		wherein        = "default:tree",
		clust_scarcity = 20*20*20,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

-- Jungle

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:ironwood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 15*15*15,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:copperwood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 14*14*14,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:goldwood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 17*17*17,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:diamondwood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 19*19*19,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})


minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:coalwood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 13*13*13,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})

minetest.register_ore({
		ore_type       = "scatter",
		ore            = "orewood:mesewood_jungle",
		wherein        = "default:jungletree",
		clust_scarcity = 20*20*20,
		clust_num_ores = 1,
		clust_size     = 1,
		y_min          = -31000,
		y_max          = 31000,
	})



-- Papynodes

minetest.register_node("orewood:ironpapy", {
	description = "Ironpapy",
	drop = "default:iron_lump",
	drawtype = "plantlike",
	stack_max = 999,
	tiles = {"ironpapy.png"},
	inventory_image = "ironpapy.png",
	wield_image = "ironpapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

minetest.register_node("orewood:copperpapy", {
	description = "Copperpapy",
	drop = "default:copper_lump",
	drawtype = "plantlike",
	stack_max = 999,
	tiles = {"copperpapy.png"},
	inventory_image = "copperpapy.png",
	wield_image = "copperpapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

minetest.register_node("orewood:coalpapy", {
	description = "Coalpapy",
	drop = 'default:coal_lump',
	drawtype = "plantlike",
	stack_max = 999,
	tiles = {"coalpapy.png"},
	inventory_image = "coalpapy.png",
	wield_image = "coalpapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

minetest.register_node("orewood:mesepapy", {
	description = "Mesepapy",
	drop = "default:mese_crystal",
	drawtype = "plantlike",
	stack_max = 999,
	tiles = {"mesepapy.png"},
	inventory_image = "mesepapy.png",
	wield_image = "mesepapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

minetest.register_node("orewood:diamondpapy", {
	description = "Diamondpapy",
	drop = "default:diamond",
	drawtype = "plantlike",
	stack_max = 999,
	tiles = {"diamondpapy.png"},
	inventory_image = "diamondpapy.png",
	wield_image = "diamondpapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

minetest.register_node("orewood:goldpapy", {
	description = "Goldpapy",
	drawtype = "plantlike",
	drop = "default:gold_lump",
	stack_max = 999,
	tiles = {"goldpapy.png"},
	inventory_image = "goldpapy.png",
	wield_image = "goldpapy.png",
	paramtype = "light",
	walkable = false,
	is_ground_content = true,
	selection_box = {
		type = "fixed",
		fixed = {-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}
	},
	groups = {snappy=3,flammable=2},
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, node, metadata, digger)
		default.dig_up(pos, node, digger)
	end,
})

-- Functions to sprout papy from logs

minetest.register_abm({
	nodenames = {"orewood:ironwood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:ironwood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:ironpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:ironwood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:ironwood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:ironpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:coalwood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:coalwood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:coalpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:coalwood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:coalwood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:coalpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:copperwood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:copperwood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:copperpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:copperwood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:copperwood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:copperpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:mesewood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:mesewood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:mesepapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:mesewood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,	action = function(pos, node)
	local height = 0
	while node.name == "orewood:mesewood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:mesepapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:diamondwood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:diamondwood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:diamondpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:diamondwood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:diamondwood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:diamondpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:goldwood"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:goldwood" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:goldpapy"})
	end,
})

minetest.register_abm({
	nodenames = {"orewood:goldwood_jungle"},
	neighbors = {"default:water_source"},
	interval = 40,
	chance = 10,
	action = function(pos, node)
	local height = 0
	while node.name == "orewood:goldwood_jungle" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:goldpapy"})
	end,
})

-- Functions to grow papy that has sprouted


minetest.register_abm({
	nodenames = {"orewood:ironpapy"},
	neighbors = {"orewood:ironwood", "orewood:ironwood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:ironwood"
	and name ~= "orewood:ironwood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:ironpapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:ironpapy"})
	return true
end,
})

minetest.register_abm({
	nodenames = {"orewood:coalpapy"},
	neighbors = {"orewood:coalwood", "orewood:coalwood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:coalwood"
	and name ~= "orewood:coalwood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:coalpapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:coalpapy"})
	return true
end,
})

minetest.register_abm({
	nodenames = {"orewood:copperpapy"},
	neighbors = {"orewood:copperwood", "orewood:copperwood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:copperwood"
	and name ~= "orewood:copperwood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:copperpapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:copperpapy"})
	return true
end,
})

minetest.register_abm({
	nodenames = {"orewood:diamondpapy"},
	neighbors = {"orewood:diamondwood", "orewood:diamondwood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:diamondwood"
	and name ~= "orewood:diamondwood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:diamondpapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:diamondpapy"})
	return true
end,
})

minetest.register_abm({
	nodenames = {"orewood:mesepapy"},
	neighbors = {"orewood:mesewood", "orewood:mesewood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:mesewood"
	and name ~= "orewood:mesewood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:mesepapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:mesepapy"})
	return true
end,
})

minetest.register_abm({
	nodenames = {"orewood:goldpapy"},
	neighbors = {"orewood:goldwood", "orewood:goldwood_jungle"},
	interval = 50,
	chance = 20,
	action = function(pos, node)
	pos.y = pos.y-1
	local name = minetest.get_node(pos).name
	if name ~= "orewood:goldwood"
	and name ~= "orewood:goldwood_jungle" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y+1
	local height = 0
	while node.name == "orewood:goldpapy" and height < 4 do
		height = height+1
		pos.y = pos.y+1
		node = minetest.get_node(pos)
	end
	if height == 4
	or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name="orewood:goldpapy"})
	return true
end,
})
