minetest.register_node("coarse:dirt", {
	description = "Coarse Dirt",
	tiles = {"coarse.png"},
	groups = {crumbly = 3, soil = 1},
})

minetest.register_craft({
	output = "coarse:dirt 4",
	recipe = {
		{"default:dirt", "default:gravel", ""},
		{"default:gravel", "default:dirt", ""},
		{"", "",  ""}
	}
})

minetest.register_node("coarse:path", {
	description = "Pathway",
	tiles = {"path.png"},
	groups = {crumbly = 3, soil = 1},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.4375, 0.5}, -- NodeBox1
		}
	}
})

minetest.register_craft({
	output = "coarse:path 4",
	recipe = {
		{"default:dirt", "default:dirt", ""},
		{"default:dirt", "default:dirt", ""},
		{"", "",  ""}
	}
})
