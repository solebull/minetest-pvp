-- GENERATED CODE
-- Node Box Editor, version 0.8.1 - Glass
-- Namespace: test

minetest.register_node("coarse:path", {
	description = "Pathway",
	tiles = {"coarse.png"},
	groups = {crumbly = 3, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.4375, 0.5}, -- NodeBox1
		}
	}
})

minetest.register_craft({
	output = "coarse:path 4",
	recipe = {
		{"default:dirt", "default:dirt", ""},
		{"default:dirt", "default:dirt", ""},
		{"", "",  ""}
	}
})

