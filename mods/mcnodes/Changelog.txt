Version 0.4:
  Fixed new large chest bug.
  Added Mchud submod and can be disable by changing the McHud variable to 0 which is located in the init.lua file (this submod is just in testing so it is not finished).

Version 0.3-beta:
  Improve some textures.
  Fix large chest issue.
  added /mcnodes command to easly get the mod version.

Version 0.2-beta:
  Textures updated.
  Set textures license (See LICENSE.txt file for more information).
  Some code improvements.


Version 0.1-beta:
  First release.
