-- Add support for technic's provided ores

if not minetest.get_modpath("technic") then return end

-- Register dignodes
for name,xp in pairs({
      -- Minerals
      ["technic:mineral_uranium"]  = 4,
      ["technic:mineral_chromium"] = 3,
      ["technic:mineral_zinc"]     = 1,
      ["technic:mineral_lead"]     = 1,
      ["technic:mineral_sulfur"]   = 2,
}) do
	xpro.register_on_dignode(name, xp)
end

