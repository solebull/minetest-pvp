# XPro v1.1.0

Forked from https://github.com/BrunoMine/xpro/
Multiple modifications and adaptations by solebull.

## Descrição _Description_
Sistema de niveis e experiencia

## Requisitos _(Requirements)_
* Minetest 0.4.17 ou superior
* Mod default
* Mod sfinv
* Mod intllib (opicional)

## Licença _(License)_
Veja LICENSE.txt para informações detalhadas da licença LGPL 3.0

### Autores do código fonte
Originalmente por BrunoMine, Bruno Borges <borgesdossantosbruno@gmail.com> (LGPL 3.0)

### Autores de mídias (texturas, modelos and sons)

Todas que não estão descritas aqui são de autoria de
BrunoMine, Bruno Borges <borgesdossantosbruno@gmail.com> (CC BY-SA 3.0)

