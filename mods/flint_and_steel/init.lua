minetest.register_tool("flint_and_steel:flint_and_steel", {
	description = "Flint and steel",
	inventory_image = "flint_and_steel.png",
	liquids_pointable = false,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={flamable = {uses=99, maxlevel=1}}
	},
	on_use = function(itemstack, user, pointed_thing)
		if pointed_thing.type == "node" and minetest.get_node(pointed_thing.above).name == "air" then
			if not minetest.is_protected(pointed_thing.above, user:get_player_name()) then
				if string.find(minetest.get_node(pointed_thing.under).name, "ice") then
					minetest.set_node(pointed_thing.above, {name="fire:basic_flame"})
				else
					minetest.set_node(pointed_thing.above, {name="fire:basic_flame"})
				end
			else
				minetest.chat_send_player(user:get_player_name(), "This area is protected!")
			end
		else
			return
		end

		itemstack:add_wear(65535/99)
		return itemstack
	end
})

minetest.register_craft({
	type = "shapeless",
	output = 'flint_and_steel:flint_and_steel',
	recipe = {"flint_and_steel:flint", "default:steel_ingot"}
})

minetest.register_craftitem("flint_and_steel:flint", {
	description = "flint",
	inventory_image = "flint_and_steel_flint.png"
})

minetest.register_node(":default:gravel", {
	description = "Gravel",
	tiles ={"default_gravel.png"},
	groups = {crumbly=2, falling_node=1},
	drop = {
			max_items = 1,
			items = {
				{
					items = {'flint_and_steel:flint'},
					rarity = 15,
				},
				{
					items = {'default:gravel'},
				}
			}
		},
		sounds = default.node_sound_dirt_defaults({
			footstep = {name="default_gravel_footstep", gain=0.45},
	}),
})
