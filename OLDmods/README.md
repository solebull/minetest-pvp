Here are removed mods and why. I do not add them to git but keep
their name for reference :

	villagers: not needed and suspected to slow down chests;
	darkage: seems to add game-time map generation;
	advanced_npc: seems to slow down gameplay;
